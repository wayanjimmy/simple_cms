class CreateSectionEdits < ActiveRecord::Migration[5.1]
  def up
    create_table :section_edits do |t|
      t.integer 'admin_user_id'
      t.integer 'section_id'
      t.string 'summary'
      t.timestamps
      t.index [:admin_user_id, :section_id]
    end
  end

  def down
    drop_table :section_edits
  end
end
